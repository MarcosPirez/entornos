using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerTPSController : MonoBehaviour
{
    public Camera cam;
    public UnityEvent onInteractionInput;
    private InputData input;
    private CharacterAnimBasedMovement characterMovement;

    public bool onInteractionZone {get; set; }

    void Start()
    {
        characterMovement = GetComponent<CharacterAnimBasedMovement>();
    }

    void Update()
    {
        if (onInteractionZone && input.jump)
        {
            onInteractionInput.Invoke();
        }
        input.getInput();
        characterMovement.moveCharacter(input.hMovement, input.vMovement, cam, input.jump, input.dash);
    }
}
